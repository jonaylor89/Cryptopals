
import base64
import binascii


def hex_to_64(n):
    byte_string = binascii.unhexlify(n)
    b64_string = base64.b64encode(byte_string)
    return b64_string


def fixed_xor(str1, str2):
    inted1 = int(str1, 16)
    inted2 = int(str2, 16)
    return str(hex(inted1 ^ inted2))[2:]
